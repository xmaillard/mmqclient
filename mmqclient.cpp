#include "mmqclient.h"

QSet<QString> MMQClient::stopWords = QSet<QString>() << "un" << "une" << "the" << "le" << "la" << "les" << "a" << "an" << "of" << "du" << "de" << "des" << "et" << "and" << "ne";
QMap<QString, QString> MMQClient::initNumberMap() {
    QMap<QString, QString> map;
    map.insert("un", "1"); map.insert("one", "1");
    map.insert("deux", "2"); map.insert("two", "2");
    map.insert("trois", "3"); map.insert("three", "3");
    map.insert("quatre", "4"); map.insert("four", "4");
    map.insert("cinq", "5"); map.insert("five", "5");
    map.insert("six", "6"); map.insert("six", "6");
    map.insert("sept", "7"); map.insert("seven", "7");
    map.insert("huit", "8"); map.insert("eight", "8");
    map.insert("neuf", "9"); map.insert("nine", "9");
    map.insert("dix", "10"); map.insert("ten", "10");
    return map;
}
QMap<QString, QString> MMQClient::numberMap = MMQClient::initNumberMap();
QMap<QString, QSet<QString> > MMQClient::initNumberLetterMap() {
    QMap<QString, QSet<QString> > map;
    map.insert("0", QSet<QString>() << "zero" << "aucun" << "none");
    map.insert("1", QSet<QString>() << "un" << "one");
    map.insert("2", QSet<QString>() << "deux" << "two" << "ii");
    map.insert("3", QSet<QString>() << "trois" << "three" << "iii");
    map.insert("4", QSet<QString>() << "quatre" << "four" << "iv");
    map.insert("5", QSet<QString>() << "cinq" << "five" << "v");
    map.insert("6", QSet<QString>() << "six" << "vi");
    map.insert("7", QSet<QString>() << "sept" << "seven" << "vii");
    map.insert("8", QSet<QString>() << "huit" << "eight" << "viii");
    map.insert("9", QSet<QString>() << "neuf" << "nine" << "ix");
    map.insert("10", QSet<QString>() << "dix" << "ten" << "x");
    map.insert("100", QSet<QString>() << "cent" << "hundred");
    return map;

}
QMap<QString, QSet<QString> > MMQClient::numberLetterMap = MMQClient::initNumberLetterMap();

MMQClient::MMQClient(QObject *parent) :
    QObject(parent),
    connected(false), playing(false), joined(false), firstReq(true),
    currentRound(-1), currentSongid(-1), lastSavedSongId(-1), lastExtractHash(), extractToPlay(), hashList(), connectedUserName(),
    currentState(Idle), gameConf(), loginData(), premonition(false), emitExtract(false), lastChatMessage(), gotSongId(), fastestPlayerIds()
{
    QDir appDir = QDir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    appDir.mkpath(appDir.absolutePath());
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(appDir.absoluteFilePath("extracts.db"));
    qDebug() << "Storing database into" << db.databaseName();
    bool result = db.open();
    if (result)
    {
        QSqlQuery* query = new QSqlQuery(db);
        query->exec("CREATE TABLE IF NOT EXISTS extract (hash TEXT, category TEXT, itemid INT UNIQUE, fieldone TEXT, fieldtwo TEXT)");//temporary
        delete query;
        query = NULL;
    }
    else
    {
        qDebug() << db.lastError().text();
    }




    pollTimer = new QTimer(this);
    pollTimer->setInterval(UPDATE_NORMAL);
    pollChatTimer = new QTimer(this);
    pollChatTimer->setInterval(UPDATE_CHAT);
    netmgr = new QNetworkAccessManager;
    netmgr->setCookieJar(new QNetworkCookieJar);

    QNetworkDiskCache *diskCache = new QNetworkDiskCache(this);
    diskCache->setCacheDirectory(appDir.absoluteFilePath("httpcache"));
    diskCache->setMaximumCacheSize(32 * (1 << 20)); // 32MB
    netmgr->setCache(diskCache);

    extractModel = new QSqlTableModel(this, db);
    extractModel->setTable("extract");
    extractModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    extractModel->select();

    connect(pollTimer, SIGNAL(timeout()), this, SLOT(pollTickRank()));
    connect(pollChatTimer, SIGNAL(timeout()), this, SLOT(pollTickChat()));

    QList<QPair<QString, QString> > lngs = getLanguages();
    for(int i = 0; i < lngs.size(); i++) {
        QNetworkRequest req = baseRequest("/welcome/categories", true, true, lngs[i].first);
        req.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
        req.setAttribute(QNetworkRequest::CacheSaveControlAttribute, true);
        QNetworkReply* rep = netmgr->get(req);
        netReplyToLangIndex[rep] = lngs[i].first;
        connect(rep, SIGNAL(finished()), SLOT(netReplyCategories()));
    }
}

MMQClient::~MMQClient()
{
    extractModel->deleteLater();
    pollTimer->deleteLater();
    netmgr->deleteLater();
    QSqlDatabase::removeDatabase("QSQLITE");
    qDebug() << "closed extract database";
}

QList<QPair<QString, QString> > MMQClient::getLanguages() const
{
    QList<QPair<QString, QString> > languages;
    languages.append(qMakePair(QString("fr"), tr("French")));
    languages.append(qMakePair(QString("en"), tr("English")));
    return languages;
}

QList<QPair<int, QString> > MMQClient::getCategories(QString &lng)
{
    return cachedCategories[lng];
}

bool MMQClient::isConnected()
{
    return connected;
}

bool MMQClient::isAnonymous()
{
    return !loginData.guestUsername.isEmpty();
}

QNetworkRequest MMQClient::baseRequest(const QString &path, bool cache_use, bool cache_save, const QString &lng) const
{
    QString lang = loginData.language;
    if(!lng.isEmpty())
        lang = lng;
    QNetworkRequest req(QUrl(QString("http://%1.massivemusicquiz.com").arg(lang) + path));
    req.setAttribute(QNetworkRequest::CacheLoadControlAttribute, cache_use ? QNetworkRequest::PreferCache : QNetworkRequest::AlwaysNetwork);
    req.setAttribute(QNetworkRequest::CacheSaveControlAttribute, cache_save);
    return req;
}

void MMQClient::netReplyCategories()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        QString lng = netReplyToLangIndex[reply];
        QJsonDocument jsonResponse = QJsonDocument::fromJson(((QString)reply->readAll()).toUtf8());
        reply->deleteLater();
        QJsonArray cats = jsonResponse.object().value("categories").toArray();
        QList<QPair<int, QString> > categories;
        for(int i = 0; i < cats.size(); i++) {
            QJsonObject ob = cats[i].toObject();
            categories.append(qMakePair(ob.value("id").toInt(), ob.value("name").toString()));
        }
        cachedCategories[lng] = categories;
        if(cachedCategories.size() == getLanguages().size()) {
            emit ready();
        }
    } else {
        qWarning() << "unable to get categories";
    }
}

void MMQClient::doLogin(LoginData loginData)
{
    this->loginData = loginData;
    if(isAnonymous()) {
        connected = true; // the cake is a lie
        emit loginSuccess();
        return;
    }
    connectedUserName = QString();
    qDebug() << "login in with" << loginData.language << loginData.email << loginData.password << loginData.category;
    QUrlQuery params;
    params.addQueryItem("email", loginData.email);
    params.addQueryItem("password", loginData.password);
    params.addQueryItem("remember_me", "1");
    params.addQueryItem("commit", "Connexion");
    QNetworkRequest req = baseRequest("/account/login");
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QNetworkReply* rep = netmgr->post(req, params.toString(QUrl::FullyEncoded).toUtf8());
    connect(rep, SIGNAL(finished()), this, SLOT(netReplyLogin()));
}

void MMQClient::netReplyLogin()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if(code == 302) {
            connected = true;
            emit loginSuccess();
            reply->deleteLater();
            return;
        }
    }
    connected = false;
    reply->deleteLater();
    emit loginFailure(InvalidCredentials);
}

void MMQClient::doLogout(bool send)
{
    connectedUserName = QString();
    if(loginData.language == NULL)
        return;
    pollTimer->stop();
    pollChatTimer->stop();
    connected = false;
    joined = false;
    playing = false;
    QNetworkReply* rep;
    if(send) {
        QNetworkRequest req = baseRequest("/account/logout");
        rep = netmgr->get(req);
    }
    netmgr->setCookieJar(new QNetworkCookieJar);
    if(send) {
        connect(rep, SIGNAL(finished()), rep, SLOT(deleteLater()));
    }
}

void MMQClient::startGame()
{
    if(!connected || joined || playing)
        return;
    qDebug() << "starting game";
    QUrlQuery params;
    params.addQueryItem("id", QString::number(loginData.category));
    if(isAnonymous()) {
        qDebug() << "anonymous login with" << loginData.guestUsername;
        params.addQueryItem("name", loginData.guestUsername);
    } else {
        params.addQueryItem("name", "");
    }
    QNetworkRequest req = baseRequest("/games");
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QNetworkReply* rep = netmgr->post(req, params.toString(QUrl::FullyEncoded).toUtf8());
    connect(rep, SIGNAL(finished()), this, SLOT(netReplyStartGame()));
}

void MMQClient::netReplyStartGame()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if(code == 200) {
            qDebug() << "let the fun begin";
            QString html = (QString)reply->readAll();
            QRegExp regTwoFields("\"two_fields\":(true|false),");
            QRegExp regName("\"player_name\":\"(.+)\",");
            QRegExp regId("\"player_id\":([0-9]+),");
            regName.setMinimal(true);
            if(!(regTwoFields.indexIn(html) > -1 && regName.indexIn(html) > -1 && regId.indexIn(html) > -1)) {
                qWarning() << "UNABLE TO RETRIEVE CONF";
                reply->deleteLater();
                return;
            }
            gameConf.round_count = MMQ_LAST_ROUND;
            gameConf.two_fields = regTwoFields.cap(1) == "true";
            connectedUserName = regName.cap(1);
            connectedUserId = regId.cap(1).toInt();
            qDebug() << "THE CONF" << gameConf.two_fields << connectedUserName << connectedUserId;
            rounds.resize(gameConf.round_count);
            rounds.fill(None);
            connected = true;
            playing = false;
            joined = false;
            pollTimer->stop();
            pollChatTimer->stop();
            pollTimer->start();
            gotSongId.clear();
            fastestPlayerIds.clear();
            if(!isAnonymous())
                pollChatTimer->start();
            emit gameInit(connectedUserName, gameConf);
            emit resultsChanged(rounds);
            emit gameStateChanged(Idle);
            reply->deleteLater();
            getStatsCache();
            return;
        } else if(code == 302 && reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString().contains("?flash_error=Pseudo")) {
            // nick already used
            reply->deleteLater();
            doLogout(false);
            emit loginFailure(NicknameInUse);
            return;
        }
    }
    qWarning() << "error reply start game";
    qWarning() << (QString)reply->readAll();
    pollTimer->stop();
    playing = false;
    reply->deleteLater();
}

void MMQClient::pollTickRank()
{
    if(!connected)
        return;
    QNetworkReply* rep = netmgr->get(baseRequest(
                                         QString("/tmp/rank_cache_%2.js?%3").arg(loginData.category).arg(getCurrentTimestamp())));
    connect(rep, SIGNAL(finished()), this, SLOT(netReplyPollRank()));
}

/*
 * (join) [=====missed round=====] (newround) [========round=======] (roundend) [==pause==] (newround) [=
 * !playing                         playing                          !playing                playing
 *  players                         players                          !players                players
 *
 * players == null -> inter-round break
 * players != null -> active round
 */
void MMQClient::netReplyPollRank()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if(code == 200) {
            QJsonDocument jsonResponse = QJsonDocument::fromJson(((QString)reply->readAll()).toUtf8());
            QJsonObject obj = jsonResponse.object();
            int time = obj.value("time").toInt(-1);
            if(time == -1) {
                qWarning() << "eived invalid JSON; skipping";
                reply->deleteLater();
                return;
            }
            bool waitTournament = obj.contains("tournament_wait");
            if(waitTournament) {
                qDebug() << "waiting for tournament";
                if(firstReq || currentState != Idle) {
                    currentState = Idle;
                    emit gameStateChanged(Idle);
                    emit timerChanged(obj.value("tournament_wait").toInt());
                }
            } else {
                bool hasPlayers = obj.contains("colors") && !obj.value("colors").isNull();
                bool hashRound = obj.contains("round_number");
                int round = -1;
                if(hashRound)
                    round = obj.value("round_number").toInt(-1);
                qDebug() << "[poll-rank] players:" << hasPlayers << "round:" << round << "time:" << time;
                if(time <= 1 && pollTimer->interval() != UPDATE_FAST)
                    pollTimer->setInterval(UPDATE_FAST);
                else if(pollTimer->interval() != UPDATE_NORMAL)
                    pollTimer->setInterval(UPDATE_NORMAL);
                if(hasPlayers) {
                    if(currentRound == -1 && round > -1) {
                        for(int i = 0; i < round; i++)
                            rounds.replace(i, Unavailable);
                        emit resultsChanged(rounds);
                    }
                    if(!joined) {
                        qDebug() << "active round BUT not yet joined";
                        // we just idle until we got the next round
                        if(firstReq || currentState != Idle) {
                            currentState = Idle;
                            if(currentRound != round) {
                                currentRound = round;
                                emit roundChanged(round);
                            }
                            fastestPlayerIds.clear();
                            emit gameStateChanged(Idle);
                            emit timerChanged(time);
                        }
                    } else {
                        if(firstReq || currentRound != round) {
                            qDebug() << "active round NEW ROUND";
                            if(emitExtract){
                                emitExtract = false;
                                emit playExtract();
                            }
                            if(round == 1) {
                                // firs round = new game
                                rounds.resize(gameConf.round_count);
                                rounds.fill(None);
                                emit gameInit(connectedUserName, gameConf);
                            }
                            currentState = Round;
                            currentRound = round;
                            joined = true;
                            playing = true;
                            fastestPlayerIds.clear();
                            emit gameStateChanged(Round);
                            emit resultsChanged(rounds);
                            emit roundChanged(round);
                            emit timerChanged(time);
                        } else {
                            qDebug() << "active round continues";
                            if(firstReq || currentState != Round) {
                                currentState = Round;
                                emit gameStateChanged(Round);
                            }
                        }
                    }
                    // parse score update
                    QJsonArray scores = obj.value("colors").toArray();
                    int pid;
                    bool has_one, has_two;
                    for(int i = 0, first = 1; i < scores.size(); ++i) {
                        QJsonArray s = scores.at(i).toArray();
                        pid = s.at(2).toInt();
                        has_one = s.at(1).toInt() == 1;
                        has_two = s.at(0).toInt() == 1;
                        if(has_one && has_two) {
                            emit playerRank(pid, first);
                            if(pid == connectedUserId)
                                emit playerSelfRank(first);
                            first++;
                        }
                        emit scoreUpdated(pid, has_one, has_two);
                    }
                } else {
                    // inter round break
                    if(currentRound > 0) {
                        if(!joined) {
                            rounds.replace(currentRound - 1, Unavailable);
                        } else if(rounds.at(currentRound - 1) == Unavailable || rounds.at(currentRound - 1) == None) {
                            rounds.replace(currentRound - 1, Empty);
                        }
                    }
                    emit resultsChanged(rounds);
                    joined = true;
                    playing = false;
                    if(firstReq || currentState != RoundBreak) {
                        currentState = RoundBreak;
                        emit gameStateChanged(currentState);
                        emit timerChanged(time);
                    }

                    qDebug() << "== BREAK!!!" << "round" << currentRound;
                    if(currentRound > 0)
                        qDebug() << "curr" << rounds.at(currentRound - 1);
                    getStatsCache();  // downloads the next extract
                }
            }
            firstReq = false;
        }
    }
    reply->deleteLater();
}

qint64 MMQClient::getCurrentTimestamp()
{
    return QDateTime::currentMSecsSinceEpoch();
}

void MMQClient::getStatsCache()
{
    if(!connected)
        return;
    QNetworkReply* rep = netmgr->get(baseRequest(QString("/tmp/stats_cache_%2.js?%3").arg(loginData.category).arg(getCurrentTimestamp())));
    connect(rep, SIGNAL(finished()), this, SLOT(netReplyPollStats()));
    QNetworkReply* rep2 = netmgr->get(baseRequest(QString("/games/alive?player_id=%2").arg(connectedUserId)));
    connect(rep2, SIGNAL(finished()), this, SLOT(netReplyPing()));
}

void MMQClient::netReplyPing()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if(code == 200) {
            QString data = ((QString)reply->readAll()).trimmed();
            if(data != "alive") {
                qWarning() << "OMG NOOOOT ALIVE:" << data;
                playing = joined = false;
                emit gameStateChanged(Idle);
                startGame();
            }
        }
    }
    reply->deleteLater();
}

void MMQClient::netReplyPollStats()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if(code == 200) {
            QJsonDocument jsonResponse = QJsonDocument::fromJson(((QString)reply->readAll()).toUtf8());
            QJsonObject obj = jsonResponse.object();
            if(!obj.value("ban").toObject().value("display_ban").isNull())
                qWarning() << "!!!! DISPLAY BAN !!!!" << obj.value("ban").toObject().value("display_ban").toString();

            bool valid = obj.value("preload").isDouble() && obj.value("finish").isObject();
            if(valid) {
                int preload = obj.value("preload").toInt();
                QJsonObject finish = obj.value("finish").toObject();

                if(!hashList.isEmpty()) {
                    int item_id = finish.value("item_id").toInt();
                    if(item_id == lastSavedSongId) {
                        qDebug() << "no saving again the same song";
                    } else {
                        QPair<QByteArray, int> hashToMatch = hashList.takeFirst();
                        if(((hashToMatch.second%MMQ_LAST_ROUND)+1) == currentRound ){
                            QString field_one = finish.value("field_one").toString();
                            QString field_two = finish.value("field_two").toString();
                            lastSavedSongId = item_id;
                            qDebug() << "got finish" << item_id << field_one << field_two;
                            ExtractInfo info = {item_id, field_one, field_two};
                            emit newExtractInfo(info);
                            QSqlRecord rec = extractModel->record();
                            rec.setValue("hash", QString(hashToMatch.first));
                            rec.setValue("category", loginData.category);
                            rec.setValue("itemid", item_id);
                            rec.setValue("fieldone", field_one);
                            if(gameConf.two_fields)
                                rec.setValue("fieldtwo", field_two);
                            qDebug() << rec;
                            Q_ASSERT(extractModel->insertRecord(-1, rec));
                            if(!extractModel->submitAll()) {
                                qWarning() << "SQL ERROR" << extractModel->lastError().text();
                                extractModel->revertAll();
                                QSqlQuery query;
                                query.prepare("UPDATE extract SET hash=:hash WHERE itemid=:itemid");
                                query.bindValue(":hash", QString(hashToMatch.first));
                                query.bindValue(":itemid", item_id);
                                if(!query.exec()){
                                    qDebug() << "SQL update error: " << query.lastError().text();
                                }

                            }
                            rec.clear();
                            lastExtractHash = QByteArray();
                        } else if(hashToMatch.second == currentRound){
                            hashList << qMakePair(QByteArray(hashToMatch.first),hashToMatch.second);
                        }else {
                            hashList = QList<QPair<QByteArray,int> >();
                            qDebug() << "!!!!!!Unable to resync hash, flushing hash list!!!!!!";
                        }
                    }
                } else {
                    qDebug() << "got finish but no hash";
                }
                downloadExtract(preload);

                // parse scores
                QString scores = obj.value("scores").toString();
                if(!scores.isNull())
                    parseScores(scores);
            } else {
                qWarning() << "invalid stats reply";
            }
        }
    }
    reply->deleteLater();
}

void MMQClient::downloadExtract(int uid)
{
    if(gotSongId.contains(uid)) {
        qDebug() << "already got song" << uid;
        return;
    }
    qDebug() << "downloading extract for" << uid;
    gotSongId.insert(uid);
    QNetworkReply* rep = netmgr->get(baseRequest(QString("/tmp/%2?%3").arg(uid).arg(getCurrentTimestamp())));
    connect(rep, SIGNAL(finished()), this, SLOT(extractDownloaded()));
}

void MMQClient::extractDownloaded()
{
    extractToPlay = QByteArray();
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        extractToPlay = QByteArray(reply->readAll());

        lastExtractHash = getExtractHash(extractToPlay);

        if(gameJoined()){
            hashList << qMakePair(QByteArray(lastExtractHash),currentRound);
        }
        emit extractReady(extractToPlay);
        emitExtract = true;
        if(premonition){
            emitExtract = false;
            emit playExtract();
        }
    }
}

void MMQClient::resolveExtract(){
    qDebug() << "last extract hash is" << lastExtractHash;

    //polling database
    QSqlQuery query;
    //qDebug() << "searching corresponding extract in database";
    query.prepare("SELECT fieldone, fieldtwo FROM extract WHERE hash=:hash");
    query.bindValue(":hash", QString(lastExtractHash));
    if(!query.exec()){
        qDebug() << "last query error: " << query.lastError().text();
    }
    else{
        if(!query.next()){
            qDebug() << "    >>>>>> no extract found <<<<<<";
        } else{
            do{
                QString answer_field_one = query.value(0).toString();
                QString answer_field_two = query.value(1).toString();
                qDebug() << "    >>>>>> found extract: " << answer_field_one << " - " << answer_field_two << "<<<<<<";
            } while(query.next());
        }
    }
}

void MMQClient::sendGuess(const QString &guess)
{
    QString clean = guess
            .toLower()
            .replace("æ", "ae", Qt::CaseInsensitive)
            .replace("œ", "oe", Qt::CaseInsensitive)
            .normalized(QString::NormalizationForm_KD)
            .remove(QRegExp("[^a-zA-Z0-9-\\s]"));
    QStringList dash = clean.split('-', QString::SkipEmptyParts);
    bool odd = true;
    for (int i = 0; i < dash.size(); ++i){
        if(odd){
            dash[i] += " ";
        }
        odd = !odd;
    }
    clean = dash.join("");
    QStringList tokens = clean.split(' ', QString::SkipEmptyParts);
    QSetIterator<QString> it(stopWords);
    while(it.hasNext())
        tokens.removeAll(it.next());
    QSet<QString> bonus;
    QStringListIterator words(tokens);
    while(words.hasNext()) {
        QString word = words.next();
        QSet<QString> more = numberLetterMap.value(word);
        bonus += more;
        QString num = numberMap.value(word);
        if(!num.isNull())
            bonus.insert(num);
    }
    tokens += bonus.toList();
//    if(tokens.size() == 0) {
//        return;
//    }
    QString token_string = tokens.join('|');
    qDebug() << "sending tokens:" << token_string;
    QUrlQuery params;
//    params.addQueryItem("_", "");
    params.addQueryItem("guess", token_string);
    QNetworkRequest req = baseRequest("/games/guess");
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QNetworkReply* rep = netmgr->post(req, params.toString(QUrl::FullyEncoded).toUtf8());
    connect(rep, SIGNAL(finished()), this, SLOT(netReplyGuess()));
}

void MMQClient::netReplyGuess()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if(code == 200) {
            QJsonDocument jsonResponse = QJsonDocument::fromJson(((QString)reply->readAll()).toUtf8());
            QJsonObject obj = jsonResponse.object();
            qDebug() << "GUESS reply" << obj;
            bool partial = obj.value("piece").toBool(false);
            RoundResult r;
            QString found = obj.value("found").toString();
            if(found == "field_one") {
                r = OnlyOne;
            } else if(found == "field_two") {
                Q_ASSERT(gameConf.two_fields);
                r = OnlyTwo;
            } else if(found == "both") {
                r = Both;
            } else {
                r = Empty;
            }
            RoundResult cur_r = rounds.at(currentRound - 1);
            if(r == Empty && (cur_r == OnlyOne || cur_r == OnlyTwo || cur_r == Both)) {
                r = cur_r;
            } else if(cur_r == Both || (cur_r == OnlyOne && r == OnlyTwo) || (cur_r == OnlyTwo && r == OnlyOne)) {
                r = Both;
            }
            rounds.replace(currentRound - 1, r);
            emit resultsChanged(rounds);
            emit guessResult(r, partial);
        }
    } else {
        qWarning() << "invalid guess reply";
        emit guessResult(Empty, false);
    }
    reply->deleteLater();
}

int MMQClient::getCurrentRound()
{
    return currentRound;
}

MMQClient::RoundState MMQClient::getCurrentState() {
    return currentState;
}

QString MMQClient::getLoggedInName()
{
    return connectedUserName;
}

QByteArray MMQClient::getExtractHash(QByteArray &data) {

    QByteArray hashData = QCryptographicHash::hash(data, QCryptographicHash::Md5);
    QByteArray res = hashData.toHex();

    return res;
}

void MMQClient::pollTickChat()
{
    QNetworkReply* rep;
    if(lastChatMessage.isNull()) {
        // first message
        QNetworkRequest req = baseRequest("/chat/send");
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        rep = netmgr->post(req, "");
        lastChatMessage = QDateTime::fromMSecsSinceEpoch(0);
    } else {
        // after that
        rep = netmgr->get(baseRequest(QString("/chat/messages/%2?%3").arg(loginData.category).arg(getCurrentTimestamp())));
    }
    connect(rep, SIGNAL(finished()), this, SLOT(netReplyChat()));
}

void MMQClient::netReplyChat()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    if (reply->error() == QNetworkReply::NoError) {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if(code == 200) {
            QString raw = ((QString)reply->readAll()).toUtf8();
            QStringList messages = raw.split('\x0c');
            QStringListIterator it(messages);
            ChatMessage m;
            while(it.hasNext()) {
                QStringList parts = it.next().split('\x08');
                int len = parts.length();
                if(!(len == 3 || len == 4))
                    // bad format
                    continue;
                m.username = parts[0];
                m.timestamp = QDateTime::fromMSecsSinceEpoch(parts[1].toDouble() * 1000);
                if(m.timestamp <= lastChatMessage)
                    // to old
                    continue;
                m.message = QString();
                if(len == 4) {
                    // event
                    if(parts[3] == "leaving") {
                        m.type = ChatMessage::Left;
                    } else {
                        continue;
                    }
                } else {
                    // message (or join event)
                    if(parts[2].isEmpty()) {
                        m.type = ChatMessage::Joined;
                    } else {
                        m.type = ChatMessage::Message;
                        m.message = parts[2];
                    }
                }
                emit chatMessage(m);
            }
            lastChatMessage = m.timestamp;
        }
    } else {
        qWarning() << "CHAT: bad response";
    }
    reply->deleteLater();
}

void MMQClient::sendChat(const QString &msg)
{
    if(!isConnected() || isAnonymous())
        return;
    QString msg2 = msg.trimmed();
    if(msg2.isEmpty())
        return;
    QUrlQuery params;
    params.addQueryItem("_", "");
    params.addQueryItem("m", msg2);
    QNetworkRequest req = baseRequest("/chat/send");
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QNetworkReply* rep = netmgr->post(req, params.toString(QUrl::FullyEncoded).toUtf8());
    connect(rep, SIGNAL(finished()), rep, SLOT(deleteLater()));
}

void MMQClient::parseScores(const QString &raw)
{
    /*
    <li><a>TEAM</a><span class='m' id='p6962993' uid='1015800'>Suavegothe</span><span class='s'>23pts</span></li>
    <li><span class='m' id='p6962768' uid='1007840'>bloodykiss in love</span><span class='s'>★19pts</span></li>
    <li><span id='p6962984'>mon pseudo</span><span class='s'>7pts</span></li>
    <li class='n'><span class='m' id='p6963022' uid='1025029'>KawaiiPinkuN</span><span class='s' style='display:none'>4pts</span></li>
    <li><span id='p6963079'>VISITERR</span><span class='s'>-1pts</span></li>
    <li class='n'><span class='m' id='p6962908' uid='1026251'>KFC_MiiZ4NiiN</span><span class='s' style='display:none'>-1pts</span></li>
    <li><span id='p6962528'>pierrerichard</span><span class='s'>★-3pts</span></li>
    */
    // we must wrap into root elem to procude valid XML
    QXmlStreamReader xml("<lis>" + raw + "</lis>");
    int state = 0;
    PlayerScore s;
    QList<PlayerScore> scores;
    while(!xml.atEnd()) {
        QXmlStreamReader::TokenType type = xml.readNext();
        QStringRef tag = xml.name();
        if(type == QXmlStreamReader::StartElement) {
            if(tag == "lis")
                continue;
            QXmlStreamAttributes attrs = xml.attributes();
            if(tag == "li" && state == 0) {
                state = 1;
            } else if(tag == "a" && state == 1) {
                // teamname
                s.team = xml.readElementText().trimmed();
                state = 2;
            } else if(tag == "span" && (state == 1 || state == 2)) {
                s.pid = attrs.value("id").mid(1).toInt();
                s.uid = -1;
                if(attrs.hasAttribute("uid"))
                    s.uid = attrs.value("uid").toInt();
                s.username = xml.readElementText();
                if(state == 1)
                    s.team = QString();
                state = 3;
            } else if(tag == "span" && state == 3 && attrs.value("class") == "s") {
                s.score = xml.readElementText().split("★").last().split("pts").first().toInt();
                s.has_fieldone = false;
                s.has_fieldtwo = false;
                s.is_self = s.pid == connectedUserId;
                scores.append(s);
                state = 0;
            } else {
                qDebug() << "score XML: unexcepted tag" << tag << "while in state" << state;
            }
        }
    }
    emit scoreReseted(scores);
}

bool MMQClient::gameJoined()
{
    return connected && joined;
}

bool MMQClient::validHttpReply(const QNetworkReply *reply, int expectedCode)
{
    if(reply->error() != QNetworkReply::NoError)
        return false;
    int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    return code == expectedCode;
}

void MMQClient::downloadCover(int itemid)
{
    QNetworkRequest req = baseRequest(QString("/images/picture/%2.jpg").arg(itemid));
    req.setAttribute(QNetworkRequest::User, itemid);
    QNetworkReply* rep = netmgr->get(req);
    connect(rep, SIGNAL(finished()), SLOT(netReplyCover()));
}

void MMQClient::netReplyCover()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(this->sender());
    int itemid = reply->request().attribute(QNetworkRequest::User).toInt();
    if(!(validHttpReply(reply, 200) || validHttpReply(reply, 301))) {
        reply->deleteLater();
        qWarning() << "error while downloading cover" << itemid;
        return;
    }
    emit coverReady(itemid, QImage::fromData(reply->readAll()));
}

void MMQClient::setPremonition(bool boolean){
    premonition = boolean;
}
