#include "logindialog.h"
#include "mmqclient.h"
#include "mainwindow.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog),
    loginData()
{
    ui->setupUi(this);
    ui->formLayout->setEnabled(false);

    QRegExp rx("^[^@]+@.+$");
    ui->emailLineEdit->setValidator(new QRegExpValidator(rx, this));

    ui->categoryComboBox->setEnabled(false);
    ui->dialogButtonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

    // generate random guest name
    QFile firstnamesFile(":/login/names/first");
    firstnamesFile.open(QIODevice::ReadOnly);
    QFile lastnamesFile(":/login/names/last");
    lastnamesFile.open(QIODevice::ReadOnly);
    QStringList firstnames = QString::fromUtf8(firstnamesFile.readAll()).split('\n');
    QStringList lastnames = QString::fromUtf8(lastnamesFile.readAll()).split('\n');
    QString fname = firstnames.at(qrand() % (firstnames.size() - 1));
    QString lname = lastnames.at(qrand() % (lastnames.size() - 1));
    fname.replace(0, 1, fname.at(0).toUpper());
    lname.replace(0, 1, lname.at(0).toUpper());

    ui->nicknameLineEdit->setText(fname + " " + lname);

    connect(ui->anonymousCheckBox, SIGNAL(stateChanged(int)), this, SLOT(checkValidForm()));
    connect(ui->languageComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(loadCategories(int)));
    connect(ui->categoryComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(checkValidForm()));
    connect(ui->nicknameLineEdit, SIGNAL(textChanged(QString)), this, SLOT(checkValidForm()));
    connect(ui->emailLineEdit, SIGNAL(textChanged(QString)), this, SLOT(checkValidForm()));
    connect(ui->passwordLineEdit, SIGNAL(textChanged(QString)), this, SLOT(checkValidForm()));

    ui->anonymousCheckBox->setChecked(true);
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::loadCategories(int index)
{
    QString lng = ui->languageComboBox->itemData(index).toString();
    ui->categoryComboBox->clear();
    QList<QPair<int, QString> > categories = dynamic_cast<MainWindow*>(parent())->getClient()->getCategories(lng);
    for(int i = 0; i < categories.size(); i++) {
        ui->categoryComboBox->addItem(categories[i].second, categories[i].first);
    }
    ui->categoryComboBox->setEnabled(true);
}

void LoginDialog::checkValidForm()
{
    bool anon = ui->anonymousCheckBox->checkState() == Qt::Checked;
    ui->emailLineEdit->setEnabled(!anon);
    ui->passwordLineEdit->setEnabled(!anon);
    ui->nicknameLineEdit->setEnabled(anon);
    bool ok;
    const int category = ui->categoryComboBox->currentData().toInt(&ok);
    const QString email = ui->emailLineEdit->text().trimmed();
    const QString pswd = ui->passwordLineEdit->text().trimmed();
    const QString nickname = ui->nicknameLineEdit->text().trimmed();
    bool acceptable = ui->emailLineEdit->hasAcceptableInput();
    bool valid = ok && ((anon && !nickname.isEmpty()) || (acceptable && !email.isEmpty() && !pswd.isEmpty()));
    if(valid) {
        loginData.language = ui->languageComboBox->currentData().toString();
        if(anon) {
            loginData.guestUsername = nickname;
            loginData.email = QString();
            loginData.password = QString();
        } else {
            loginData.guestUsername = QString();
            loginData.email = email;
            loginData.password = pswd;
        }
        loginData.category = category;
    }
    ui->dialogButtonBox->button(QDialogButtonBox::Ok)->setEnabled(valid);
}

void LoginDialog::setLanguages(QList<QPair<QString, QString> > lngs)
{
    for(int i = 0; i < lngs.size(); i++) {
        ui->languageComboBox->addItem(lngs.at(i).second, lngs.at(i).first);
    }
    ui->formLayout->setEnabled(true);
}

MMQClient::LoginData LoginDialog::getLoginData() const
{
    return loginData;
}
