#ifndef PLAYERSCOREMODEL_H
#define PLAYERSCOREMODEL_H

#include "../mmqclient.h"
#include <QAbstractTableModel>
#include <QFont>
#include <QPixmap>

class PlayerScoreModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit PlayerScoreModel(QObject *parent = 0);

    void clear(const QModelIndex &parent=QModelIndex());
    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    int columnCount(const QModelIndex &parent=QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool insertRows(int row, int count, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);

private:
    QMap<int,int> _ranks;
    QList<MMQClient::PlayerScore> _data;
    QMap<int, int> _pidToRow;

signals:

public slots:
    void setScores(QList<MMQClient::PlayerScore> &scorelist);
    void updateScore(int pid, bool has_fieldone, bool has_fieldtwo);
    void setRank(int pid, int rank);
    void clearRanks();

};

#endif // PLAYERSCOREMODEL_H
