#include "songhistorymodel.h"

SongHistoryModel::SongHistoryModel(QObject *parent) :
    QAbstractListModel(parent), _data(), _covers(), _songIdToRow()
{
}

int SongHistoryModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _data.count();
}

void SongHistoryModel::clear(const QModelIndex &parent)
{
    if(_data.count() > 0) {
        beginRemoveRows(QModelIndex(), 0, _data.count());
        _data.clear();
        _songIdToRow.clear();
        _covers.clear();
        endRemoveRows();
        emit dataChanged(parent, parent);
    }
}

bool SongHistoryModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginInsertRows(QModelIndex(), row, row + count);
    for(int i = 0; i < count; i++) {
        _data.insert(row, MMQClient::ExtractInfo());
        _covers.insert(row, QImage());
    }
    endInsertRows();
    return true;
}

bool SongHistoryModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    if(count == 0)
        return true;
    beginRemoveRows(QModelIndex(), row, row + count);
    for(int i = 0; i < count; i++) {
        _songIdToRow.remove(_data.at(row).item_id);
        _data.removeAt(row);
        _covers.removeAt(row);
    }
    endRemoveRows();
    return true;
}

QVariant SongHistoryModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    const MMQClient::ExtractInfo info = _data.at(index.row());
    switch(role) {
    case Qt::DisplayRole:
        return info.fieldone;
    case Qt::UserRole:
        return info.fieldtwo;
    case Qt::UserRole + 1:
        return info.item_id;
    case Qt::DecorationRole:
        return _covers.at(index.row());
    }
    return QVariant();
}

void SongHistoryModel::addExtractInfo(MMQClient::ExtractInfo info)
{
    int c = rowCount();
    if(c >= MAX_EXTRACT_INFO){
        removeRows(0, 1, QModelIndex());
    }
    c = rowCount();
    insertRows(c, 1, QModelIndex());
    _data.replace(c, info);
    _songIdToRow[info.item_id] = c;
    qDebug() << "inserting row at" << c << "with itemid" << info.item_id;
    emit dataChanged(index(c, 0), index(c, 0));
}

void SongHistoryModel::coverReady(int itemid, QImage cover)
{
    qDebug() << "got cover ready" << itemid << "null?" << cover.isNull() << cover.height();
    int row = _songIdToRow[itemid];
    qDebug() << "for itemid" << itemid << "i got row" << row;
    _covers.replace(row, cover);
    QVector<int> v;
    v.append(Qt::DecorationRole);
    emit dataChanged(index(row), index(row), v);
}
