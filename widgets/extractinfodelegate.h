#ifndef EXTRACTINFODELEGATE_H
#define EXTRACTINFODELEGATE_H

#include <QDebug>
#include <QPainter>
#include <QStyledItemDelegate>

class ExtractInfoDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit ExtractInfoDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option,
                                 const QModelIndex &index) const;
signals:

public slots:

};

#endif // EXTRACTINFODELEGATE_H
