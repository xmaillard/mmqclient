#include "validitylabel.h"

ValidityLabel::ValidityLabel(QWidget *parent) :
    QWidget(parent)
{
    layout = new QHBoxLayout(this);
    label = new QLabel(this);
    icon = new QLabel(this);
    icon_valid = new QPixmap(":/main/valid");
    icon_invalid = new QPixmap(":/main/invalid");
    icon->setPixmap(*icon_invalid);
    layout->addWidget(icon);
    layout->addWidget(label);
    layout->setMargin(0);
    setLayout(layout);
}

ValidityLabel::~ValidityLabel()
{
    delete label;
    delete icon;
    delete icon_valid;
    delete icon_invalid;
    delete layout;
}

void ValidityLabel::setValid(bool valid)
{
    this->valid = valid;
    icon->setPixmap(valid ? *icon_valid : *icon_invalid);
}

void ValidityLabel::setLabel(const QString &text)
{
    label->setText(text);
}
