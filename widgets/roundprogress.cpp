#include "roundprogress.h"

RoundProgress::RoundProgress(QWidget *parent) :
    QWidget(parent), height(BASE_HEIGHT), currentRound(-1), gameConfig(), rounds(), c(0)
{
    timer = new QTimer(this);
    timer->setInterval(100);
    timer->start();
    connect(timer, SIGNAL(timeout()), SLOT(forceRepaint()));
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
    reset();
}


RoundProgress::~RoundProgress()
{
    delete timer;
}

void RoundProgress::reset()
{
    gameConfig.round_count = 0;
    gameConfig.two_fields = true;
    setGameConfig(gameConfig);
}

QSize RoundProgress::sizeHint() const
{
    int w = (height + 4) * gameConfig.round_count;
    return QSize(w, height + 4);
}

void RoundProgress::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QRect r(QPoint(0, 0), sizeHint());
    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing, true);
    p.setRenderHint(QPainter::SmoothPixmapTransform, true);
    p.setRenderHint(QPainter::HighQualityAntialiasing, true);
    p.translate(2, 2);

    p.setPen(QPen(Qt::black, 1));
    for(int i = 0; i < rounds.size(); i++) {
        QRect rect = QRect(0, 0, height, height).translated((height + 4) * i, 0);
        QColor color = colorForResult(rounds.at(i));
        if(i + 1 == currentRound) {
            if(rounds.at(i) == MMQClient::None)
                color = Qt::white;
            color.setAlpha(50 + (.5 + qSin(c / 2.) / 2.) * 205);
        }
        p.setBrush(QBrush(color));
        p.drawRoundedRect(rect, 3, 3);
        int rank = ranks.at(i);
        p.setBrush(Qt::black);
        if(rank != -1)
            p.drawText(rect, Qt::AlignCenter, QString::number(rank));
    }
}

QColor RoundProgress::colorForResult(MMQClient::RoundResult r)
{
    if(r == MMQClient::Unavailable)
        return Qt::white;
    else if(r == MMQClient::None)
        return Qt::transparent;
    else if(r == MMQClient::Empty)
        return Qt::red;
    else if(r == MMQClient::OnlyOne)
        return Qt::cyan;
    else if(r == MMQClient::OnlyTwo)
        return Qt::blue;
    else if(r == MMQClient::Both)
        return Qt::green;
    return Qt::magenta;
}

void RoundProgress::setGameConfig(const MMQClient::GameConfig conf)
{
    gameConfig = conf;
    rounds.resize(conf.round_count);
    rounds.fill(MMQClient::None);
    ranks.resize(conf.round_count);
    ranks.fill(-1);
    repaint();
    updateGeometry();
}

void RoundProgress::setResults(const QVector<MMQClient::RoundResult> rounds)
{
    Q_ASSERT(rounds.size() == gameConfig.round_count);
    this->rounds = rounds;
    qDebug() << "ROUNDS" << rounds;
    repaint();
}

void RoundProgress::setHeight(int height)
{
    this->height = height;
    repaint();
    updateGeometry();
}

void RoundProgress::setCurrentRound(int round)
{
    this->currentRound = round;
    repaint();
}

void RoundProgress::forceRepaint()
{
    c++;
    repaint();
}

void RoundProgress::setRank(int round, int rank)
{
    Q_ASSERT(0 <= round && round < gameConfig.round_count);
    ranks.replace(round, rank);
    repaint();
}
