#include "mmqclient.h"
#include "mainwindow.h"
#include "logindialog.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    QString locale = QLocale::system().name().split('_').first();

    QTranslator translator;
    translator.load(QString("mmqclient_") + locale);
    app.installTranslator(&translator);

    MMQClient mmq;

    MainWindow main(&mmq);
    main.show();

    return app.exec();
}
